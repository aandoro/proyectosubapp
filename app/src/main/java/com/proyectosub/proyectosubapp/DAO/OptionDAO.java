package com.proyectosub.proyectosubapp.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.proyectosub.proyectosubapp.models.Group;
import com.proyectosub.proyectosubapp.models.Option;
import com.proyectosub.proyectosubapp.models.Trivia;

import java.util.List;

@Dao
public interface OptionDAO {
    @Query("SELECT * FROM options")
    public List<Option> getOptions();

    @Query("SELECT * FROM options WHERE id = :id")
    public Option getOptionById(Long id);

    @Insert
    public void insert(Option... options);

    @Update
    public void update(Option... options);

    @Delete
    public void delete(Option option);

    @Query("SELECT * FROM options WHERE triviaId = :triviaId")
    public List<Option> getOptionsByTrivia(int triviaId);
}
