package com.proyectosub.proyectosubapp.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.proyectosub.proyectosubapp.models.Image;

import java.util.List;

@Dao
public interface ImageDAO {

    @Query("SELECT * FROM images")
    public List<Image> getImages();

    @Query("SELECT * FROM images WHERE id = :id")
    public Image getImageById(Long id);

    @Insert
    public void insert(Image... Images);

    @Update
    public void update(Image... Images);

    @Delete
    public void delete(Image image);

    @Query("SELECT * FROM images WHERE send = :send")
    public List<Image> getImageBySend(boolean send);
}
