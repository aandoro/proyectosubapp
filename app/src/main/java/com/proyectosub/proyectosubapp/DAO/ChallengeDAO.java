package com.proyectosub.proyectosubapp.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.proyectosub.proyectosubapp.models.Challenge;

import java.util.List;

@Dao
public interface ChallengeDAO {
    @Query("SELECT * FROM challenges")
    public List<Challenge> getChallenges();

    @Query("SELECT * FROM challenges WHERE id = :id")
    public Challenge getChallengeById(int id);

    @Insert
    public void insert(Challenge... challenges);

    @Update
    public void update(Challenge... challenges);

    @Delete
    public void delete(Challenge specie);

    @Query("SELECT * FROM challenges WHERE levelId = :levelId")
    public List<Challenge> getChallengesByLevel(int levelId);
}
