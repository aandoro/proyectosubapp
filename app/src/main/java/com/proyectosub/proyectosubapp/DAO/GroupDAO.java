package com.proyectosub.proyectosubapp.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.proyectosub.proyectosubapp.models.Group;

import java.util.List;

@Dao
public interface GroupDAO {
    @Query("SELECT * FROM groups")
    public List<Group> getGroups();

    @Query("SELECT * FROM groups WHERE id = :id")
    public Group getGroupById(Long id);

    @Insert
    public void insert(Group... groups);

    @Update
    public void update(Group... groups);

    @Delete
    public void delete(Group group);

}
