package com.proyectosub.proyectosubapp.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.proyectosub.proyectosubapp.models.Trivia;

import java.util.List;

@Dao
public interface TriviaDAO {

    @Query("SELECT * FROM trivias")
    public List<Trivia> getTrivias();

    @Query("SELECT * FROM trivias WHERE id = :id")
    public Trivia getTriviaById(int id);

    @Insert
    public void insert(Trivia... trivias);

    @Update
    public void update(Trivia... trivias);

    @Delete
    public void delete(Trivia trivia);

    @Query("SELECT * FROM trivias WHERE levelId = :levelId")
    public List<Trivia> getTriviaByLevel(int levelId);
}
