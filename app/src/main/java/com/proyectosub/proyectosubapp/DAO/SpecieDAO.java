package com.proyectosub.proyectosubapp.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.proyectosub.proyectosubapp.models.Group;
import com.proyectosub.proyectosubapp.models.Specie;

import java.util.List;

@Dao
public interface SpecieDAO {
    @Query("SELECT * FROM species")
    public List<Specie> getSpecies();

    @Query("SELECT * FROM species WHERE id = :id")
    public Specie getSpecieById(int id);

    @Insert
    public void insert(Specie... species);

    @Update
    public void update(Specie... species);

    @Delete
    public void delete(Specie specie);

    @Query("SELECT * FROM species WHERE groupId = :groupId")
    public List<Specie> getSpecieByGroup(int groupId);
}
