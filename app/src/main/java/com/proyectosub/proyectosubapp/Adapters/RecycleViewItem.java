package com.proyectosub.proyectosubapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.models.UserMobile;

import java.util.List;

public class RecycleViewItem extends RecyclerView.Adapter {
    private Context context;
    private List<UserMobile> listUserMobile;

    public RecycleViewItem(Context context, List<UserMobile> listUserMobile) {
        this.context = context;
        this.listUserMobile = listUserMobile;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(context).inflate(R.layout.item_list, null);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        UserMobile dir = listUserMobile.get(position);
        Holder holder1 = (Holder) holder;

        holder1.place.setText(String.valueOf(position + 1));
        holder1.user.setText(dir.getName());
        holder1.score.setText(String.valueOf(dir.getPoint()));
    }

    @Override
    public int getItemCount() {
        return listUserMobile.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView place;
        TextView user;
        TextView score;

        public Holder(@NonNull View itemView) {
            super(itemView);
            place = itemView.findViewById(R.id.puesto_num);
            user = itemView.findViewById(R.id.username);
            score = itemView.findViewById(R.id.user_score);
        }
    }
}