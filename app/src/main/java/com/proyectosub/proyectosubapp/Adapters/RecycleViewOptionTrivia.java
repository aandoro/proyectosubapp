package com.proyectosub.proyectosubapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.models.Option;

import java.util.List;

public class RecycleViewOptionTrivia extends RecyclerView.Adapter {
    private Context context;
    private List<Option> listOption;
    private boolean lastItemSelection = false;
    private int itemSelection = 5;

    public RecycleViewOptionTrivia(Context context, List<Option> listOption) {
        this.context = context;
        this.listOption = listOption;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(context).inflate(R.layout.item_list_trivia, null);

        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        Option dir = listOption.get(position);
        final Holder holder1 = (Holder) holder;

        holder1.checkBox.setText(dir.getOption());
        holder1.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //handle the boolean flag here.
                if ((b == true) && (itemSelection != position)) {

                    if (lastItemSelection) {
                        holder1.checkBox.setChecked(false);
                    } else {
                        itemSelection = position;
                        Log.i("OPCIONMARCADA: ", Integer.toString(itemSelection));
                        lastItemSelection = true;
                    }

                } else {
                    holder1.checkBox.setChecked(false);
                    lastItemSelection = false;
                    itemSelection = 5;
                    Log.i("OPCIONDESMARCADA: ", Integer.toString(itemSelection));
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return listOption.size();
    }

    public String opcionElegida() {
        String ultimaOpcion = listOption.get(itemSelection).getOption();
        return ultimaOpcion;

    }

    public int getItemSelection() {
        return itemSelection;
    }

    public class Holder extends RecyclerView.ViewHolder {
        CheckBox checkBox;

        Holder(@NonNull View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.option_check);
        }
    }
}
