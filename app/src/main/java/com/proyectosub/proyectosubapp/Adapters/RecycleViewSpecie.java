package com.proyectosub.proyectosubapp.Adapters;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.models.Specie;

import java.util.List;

/**
 * Adapter used to show a simple grid of products.
 */
public class RecycleViewSpecie extends RecyclerView.Adapter {

    private List<Specie> specieList;

    private Specie specie;


    public RecycleViewSpecie(List<Specie> specieList) {
        this.specieList = specieList;


    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.shr_specie_card, parent, false);


        return new Holder(layoutView);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        specie = specieList.get(position);
        Holder holder1;


        holder1 = (Holder) holder;
        holder1.tittle.setText(specie.getName());
        holder1.description.setText(specie.getScientificName());
        Bitmap bitmap = BitmapFactory.decodeByteArray(specie.getImage(), 0, specie.getImage().length);
        holder1.specieImage.setImageBitmap(bitmap);

        // agregado


        //    holder1.setOnClickListener(new View.OnClickListener() {
        //        @Override
        //         public void onClick(View v) {

        //       }
        //    });


        // hasta aca

    }

    @Override
    public int getItemCount() {
        return specieList.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        ImageView specieImage;
        TextView tittle;
        TextView description;


        public Holder(@NonNull View itemView) {
            super(itemView);
            specieImage = (ImageView) itemView.findViewById(R.id.imageSpecie);
            tittle = (TextView) itemView.findViewById(R.id.titleSpecie);
            description = (TextView) itemView.findViewById(R.id.descriptionSpecie);

        }
    }

}