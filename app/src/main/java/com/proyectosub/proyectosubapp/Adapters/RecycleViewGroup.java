package com.proyectosub.proyectosubapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.controllers.SpecieFragment;
import com.proyectosub.proyectosubapp.models.Group;
import com.proyectosub.proyectosubapp.models.SimpleDialog;

import java.util.List;

public class RecycleViewGroup extends RecyclerView.Adapter {

    private FragmentActivity fragmentActivity;
    private Context context;
    private List<Group> listGroup;

    public RecycleViewGroup(FragmentActivity fragmentActivity, Context context, List<Group> listGroup) {
        this.fragmentActivity = fragmentActivity;
        this.context = context;
        this.listGroup = listGroup;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(context).inflate(R.layout.card_list_group, null);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Group dir = listGroup.get(position);
        Holder holder1 = (Holder) holder;

        holder1.groupTitle.setText(dir.getName());
        holder1.see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new SpecieFragment();
                replaceFragment(fragment);
                ((SpecieFragment) fragment).setGroupId(listGroup.get(position).getId());

            }
        });
    }

    private void replaceFragment(Fragment fragment) {


        FragmentTransaction transaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
        //getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);

        // Commit a la transacción
        transaction.commit();
    }


    @Override
    public int getItemCount() {
        return listGroup.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
    //    CardView cardView;
       // ImageView groupImage;
        TextView groupTitle;
        Button see;

        public Holder(@NonNull View itemView) {
            super(itemView);
        //    cardView = itemView.findViewById(R.id.cardView);
         //   groupImage = (ImageView) itemView.findViewById(R.id.product_image);
            groupTitle = (TextView) itemView.findViewById(R.id.product_title);
            see = itemView.findViewById(R.id.group_details);
        }
    }
}
