package com.proyectosub.proyectosubapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.proyectosub.proyectosubapp.Utils.SharedPrefUtils;
import com.proyectosub.proyectosubapp.controllers.CatalogFragment;
import com.proyectosub.proyectosubapp.controllers.DetailsProfileFragment;
import com.proyectosub.proyectosubapp.controllers.GameFragment;
import com.proyectosub.proyectosubapp.controllers.HomeFragment;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_PERMISSION = 200;
    private TextView title, description;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    break;
                case R.id.navigation_catalog:
                    fragment = new CatalogFragment();
                    break;
                case R.id.navigation_game:
                    fragment = new GameFragment();
                    break;
                case R.id.navigation_profile:
                  //  fragment = new ProfileFragment();
                    fragment = new DetailsProfileFragment();
                    break;
            }
            replaceFragment(fragment);
            return true;
        }
    };

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_placeholder, fragment);
        fragmentTransaction.commit();
    }

    private void setInitialFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_placeholder, new HomeFragment());
        fragmentTransaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, REQUEST_PERMISSION);
        }

        BottomNavigationView navView = findViewById(R.id.bottom_navigation);

        configurationMenu(navView);

        setInitialFragment();

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void configurationMenu(BottomNavigationView navView) {

        String user = SharedPrefUtils.get(getApplicationContext(), "username");

        if (user.equals("null")){
            navView.getMenu().findItem(R.id.navigation_profile).setVisible(false);
        }
    }


}
