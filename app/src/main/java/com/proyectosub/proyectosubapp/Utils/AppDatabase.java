package com.proyectosub.proyectosubapp.Utils;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.proyectosub.proyectosubapp.DAO.CategoryDAO;
import com.proyectosub.proyectosubapp.DAO.ChallengeDAO;
import com.proyectosub.proyectosubapp.DAO.GroupDAO;
import com.proyectosub.proyectosubapp.DAO.ImageDAO;
import com.proyectosub.proyectosubapp.DAO.LevelDAO;
import com.proyectosub.proyectosubapp.DAO.LocationDAO;
import com.proyectosub.proyectosubapp.DAO.OptionDAO;
import com.proyectosub.proyectosubapp.DAO.SpecieDAO;
import com.proyectosub.proyectosubapp.DAO.TriviaDAO;
import com.proyectosub.proyectosubapp.models.Category;
import com.proyectosub.proyectosubapp.models.Challenge;
import com.proyectosub.proyectosubapp.models.Group;
import com.proyectosub.proyectosubapp.models.Image;
import com.proyectosub.proyectosubapp.models.Level;
import com.proyectosub.proyectosubapp.models.MyLocation;
import com.proyectosub.proyectosubapp.models.Option;
import com.proyectosub.proyectosubapp.models.Specie;
import com.proyectosub.proyectosubapp.models.Trivia;

@Database(entities = {Image.class, MyLocation.class, Group.class, Specie.class, Level.class, Challenge.class, Trivia.class, Option.class}, version = 5, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ImageDAO getImageDAO();

    public abstract LocationDAO getLocationDAO();

    public abstract GroupDAO getGroupDAO();

    public abstract SpecieDAO getSpecieDAO();

    public abstract LevelDAO getLevelDAO();

//    public abstract CategoryDAO getCategoryDAO();

    public abstract ChallengeDAO getChallengeDAO();

    public abstract TriviaDAO getTriviaDAO();

    public abstract OptionDAO getOptionDAO();
}
