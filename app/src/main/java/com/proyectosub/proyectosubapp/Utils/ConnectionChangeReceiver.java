package com.proyectosub.proyectosubapp.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.proyectosub.proyectosubapp.App;

public class ConnectionChangeReceiver extends BroadcastReceiver {

    private ControllerConnection ctrlConnection;

    @Override
    public void onReceive(Context context, Intent intent) {
        ctrlConnection = new ControllerConnection(context);
        boolean goodConex = ctrlConnection.comprobarBuenaConexion();
        App.getInstance().setGoodConecction(goodConex);
    }
}
