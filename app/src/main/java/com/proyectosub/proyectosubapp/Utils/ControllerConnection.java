package com.proyectosub.proyectosubapp.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.proyectosub.proyectosubapp.App;

public class ControllerConnection {

    private static final String TAG = "A_CONEX";

    private ConnectivityManager connectivityManager;
    private App app;

    public ControllerConnection(Context context) {
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public synchronized boolean comprobarBuenaConexion() {
        Log.d("NETWORK", String.valueOf(connectivityManager.isDefaultNetworkActive()));
        if (!connectivityManager.isDefaultNetworkActive()) return false;
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork == null) return false;
//        Log.d(TAG, String.valueOf(activeNetwork.getType()));
//        Log.d(TAG, String.valueOf(activeNetwork.isConnectedOrConnecting()));
        app = App.getInstance();

        if (activeNetwork.isConnectedOrConnecting()) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                Log.d(TAG, "HAY WIFI");
                return true;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                int subType = activeNetwork.getSubtype();
                switch (subType) {
                    case TelephonyManager.NETWORK_TYPE_UMTS:
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                    case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                    case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                    case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
                        Log.d(TAG, "HAY 3G");
                        return true; // 3G
                    //break;
                    case TelephonyManager.NETWORK_TYPE_LTE:
                        Log.d(TAG, "HAY 4G");
                        return true; // 4G
                    //break;
                    default:
                        Log.d(TAG, "NO HAY NI WIFI, NI 3G Ni 4G, hay Red de tipo " + activeNetwork.getTypeName() + ", (" + activeNetwork.getSubtypeName() + ") conectada");
                        return false; // hay red de mala calidad
                    //break;
                }
            } else {
                Log.d(TAG, "NO HAY RED MOVIL");
                return false;
            }
        } else {
            Log.d(TAG, "NO HAY  NINGUNA RED");
            return false;
        }
    } // fin comprobarBuenaConexion()
}
