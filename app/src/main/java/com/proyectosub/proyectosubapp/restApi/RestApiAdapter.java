package com.proyectosub.proyectosubapp.restApi;

import com.proyectosub.proyectosubapp.Utils.Connection;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestApiAdapter {

    public RestApi connectionEnable() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Connection.URL_WEB_SERVICES)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(RestApi.class);
    }
}
