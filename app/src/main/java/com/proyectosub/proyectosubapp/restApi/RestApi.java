package com.proyectosub.proyectosubapp.restApi;


import com.proyectosub.proyectosubapp.Utils.Connection;
import com.proyectosub.proyectosubapp.models.Challenge;
import com.proyectosub.proyectosubapp.models.Level;
import com.proyectosub.proyectosubapp.models.Success;
import com.proyectosub.proyectosubapp.models.Trivia;
import com.proyectosub.proyectosubapp.models.UserMobile;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RestApi {

    @FormUrlEncoded
    @POST(Connection.URL_WEB_SERVICES + "login")
    Call<Success> login(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST(Connection.URL_WEB_SERVICES + "register")
    Call<UserMobile> register(
            @Field("name") String name,
            @Field("surname") String surname,
            @Field("username") String username,
            @Field("email") String email,
            @Field("password") String password,
            @Field("c_password") String c_password,
            @Field("role") int role);

    @FormUrlEncoded
    @POST(Connection.URL_WEB_SERVICES + "data")
    Call<UserMobile> dataUser(
            @Field("username") String username);

    @FormUrlEncoded
    @PUT(Connection.URL_WEB_SERVICES + "userDataMovil" + "/{id}")
    Call<UserMobile> editProfile(
            @Path("id") int id,
            @Field("name") String name,
            @Field("surname") String surname,
            @Field("username") String username,
            @Field("email") String email,
            @Field("point") int point);


    @GET(Connection.URL_WEB_SERVICES + "challenges/level/{level}")
    Call<Challenge> getChallenges(
            @Path("level") int level);



    @GET(Connection.URL_WEB_SERVICES + "trivias/level/{level}")
    Call<Trivia> getTrivias(
            @Path("level") int level);


    @GET(Connection.URL_WEB_SERVICES + "leveles")
    Call<Level> getLeveles();

}
