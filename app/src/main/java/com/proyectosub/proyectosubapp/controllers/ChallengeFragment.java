package com.proyectosub.proyectosubapp.controllers;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Rational;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageAnalysisConfig;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.asksira.bsimagepicker.BSImagePicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.proyectosub.proyectosubapp.App;
import com.proyectosub.proyectosubapp.DAO.ImageDAO;
import com.proyectosub.proyectosubapp.DAO.LocationDAO;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.AppDatabase;
import com.proyectosub.proyectosubapp.models.Image;
import com.proyectosub.proyectosubapp.models.MyLocation;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class ChallengeFragment extends Fragment implements BSImagePicker.OnMultiImageSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private View challengeFragment;
    private int challengeId;
    private FloatingActionButton buttonSend;
    private int picCountChallenge;
    private ImageButton captureButton;
    private ImageCapture imgCap;
    private TextureView txView;
    private int REQUEST_CODE_PERMISSIONS = 200; //arbitrary number, can be changed accordingly
    private AppDatabase database;
    private GoogleApiClient mGoogleApiClient;
    private String formattedDateInit;

    private Button.OnClickListener bButtonSend = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            click();
        }
    };
    private Button.OnClickListener bCaptureButton = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final String nameFile = Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES + "/PS_" + System.currentTimeMillis() + ".jpg";
            File file = new File(nameFile);
            imgCap.takePicture(file, new ImageCapture.OnImageSavedListener() {
                @Override
                public void onImageSaved(@NonNull File file) {
                    float anchoMax = 800;
                    Bitmap b = BitmapFactory.decodeFile(file.getAbsolutePath());
                    Log.d("WIDTH ", String.valueOf(b.getWidth()));
                    Log.d("HEIGHT ", String.valueOf(b.getHeight()));

                    float aspecto = (b.getWidth() * 1.0f) / (b.getHeight() * 1.0f);
                    float altoCalculato = anchoMax * aspecto;
                    Bitmap out = Bitmap.createScaledBitmap(b, (int)altoCalculato, (int)altoCalculato, false);

                    FileOutputStream fOut;
                    try {
                        fOut = new FileOutputStream(file);
                        out.compress(Bitmap.CompressFormat.JPEG, 50, fOut);
                        fOut.flush();
                        fOut.close();
                        b.recycle();
                        out.recycle();
                    } catch (Exception e) {

                    }
                    String msg = "Foto guardada con exito";
                    Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
                    getLocation(nameFile);
                    galleryAddPic(file);
                }

                @Override
                public void onError(@NonNull ImageCapture.UseCaseError useCaseError, @NonNull String message, @Nullable Throwable cause) {
                    String msg = "Fallo en la captura de la foto: " + message;
                    Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
                    if (cause != null) {
                        cause.printStackTrace();
                    }
                }
            });

        }
    };

    private void galleryAddPic(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        challengeFragment.getContext().sendBroadcast(mediaScanIntent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        challengeFragment = inflater.inflate(R.layout.fragment_challenge, container, false);

        init();

        buttonSend.setOnClickListener(bButtonSend);

        if (!allPermissionsGranted()) {
            startCamera();
        } else {
            if (allPermissionsGranted()) {
                requestPermissionAndContinue();
            } else {
                startCamera();
            }
        }

        return challengeFragment;
    }

    private void init() {

        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        formattedDateInit = df.format(currentTime);
        buttonSend = challengeFragment.findViewById(R.id.btn_send);
        captureButton = challengeFragment.findViewById(R.id.capture_button);
        String imageFilePath = "";
        ImageCaptureConfig imgCConfig = new ImageCaptureConfig.Builder().setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                .setTargetRotation(getActivity().getWindowManager().getDefaultDisplay().getRotation()).build();
        imgCap = new ImageCapture(imgCConfig);
        txView = challengeFragment.findViewById(R.id.view_finder);
        database = Room.databaseBuilder(getContext(), AppDatabase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(challengeFragment.getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    private void requestPermissionAndContinue() {
        if (ContextCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), WRITE_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                alertBuilder.setCancelable(true);
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSIONS);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();
                Log.e("", "permission denied, show dialog");
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE,}, REQUEST_CODE_PERMISSIONS);
            }
        } else {
            startCamera();
        }
    }

    private void updateTransform() {
        //compensates the changes in orientation for the viewfinder, bc the rest of the layout stays in portrait mode.
        //methinks :thonk:
        Matrix mx = new Matrix();
        float w = txView.getMeasuredWidth();
        float h = txView.getMeasuredHeight();

        float cX = w / 2f; //calc centre of the viewfinder
        float cY = h / 2f;

        int rotationDgr;
        int rotation = (int) txView.getRotation(); //cast to int bc switches don't like floats

        switch (rotation) { //correct output to account for display rotation
            case Surface.ROTATION_0:
                rotationDgr = 0;
                break;
            case Surface.ROTATION_90:
                rotationDgr = 90;
                break;
            case Surface.ROTATION_180:
                rotationDgr = 180;
                break;
            case Surface.ROTATION_270:
                rotationDgr = 270;
                break;
            default:
                return;
        }

        mx.postRotate((float) rotationDgr, cX, cY);
        txView.setTransform(mx); //apply transformations to textureview
    }

    private void startCamera() {
        //make sure there isn't another camera instance running before starting
        CameraX.unbindAll();

        /* start preview */
        int aspRatioW = txView.getWidth(); //get width of screen
        int aspRatioH = txView.getHeight(); //get height
        Rational asp = new Rational(aspRatioW, aspRatioH); //aspect ratio
        Size screen = new Size(aspRatioW, aspRatioH); //size of the screen

        //config obj for preview/viewfinder thingy.
        PreviewConfig pConfig = new PreviewConfig.Builder().setTargetAspectRatio(asp).setTargetResolution(screen).build();
        Preview preview = new Preview(pConfig); //lets build it

        preview.setOnPreviewOutputUpdateListener(
                new Preview.OnPreviewOutputUpdateListener() {
                    //to update the surface texture we  have to destroy it first then re-add it
                    @Override
                    public void onUpdated(Preview.PreviewOutput output) {
                        ViewGroup parent = (ViewGroup) txView.getParent();
                        parent.removeView(txView);
                        parent.addView(txView, 0);

                        txView.setSurfaceTexture(output.getSurfaceTexture());
                        updateTransform();
                    }
                });


        /* image capture */
        captureButton.setOnClickListener(bCaptureButton);

        /* image analyser */

        ImageAnalysisConfig imgAConfig = new ImageAnalysisConfig.Builder().setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE).build();
        ImageAnalysis analysis = new ImageAnalysis(imgAConfig);

        analysis.setAnalyzer(
                new ImageAnalysis.Analyzer() {
                    @Override
                    public void analyze(ImageProxy image, int rotationDegrees) {
                        //y'all can add code to analyse stuff here idek go wild.
                    }
                });


        //bind to lifecycle:
        CameraX.bindToLifecycle(this, analysis, imgCap, preview);
    }

    private void click() {

        BSImagePicker multiSelectionPicker = new BSImagePicker.Builder(challengeFragment.getContext().getPackageName() + ".provider")
                .isMultiSelect() //Set this if you want to use multi selection mode.
                .setMinimumMultiSelectCount(picCountChallenge) //Default: 1.
                .setMaximumMultiSelectCount(picCountChallenge) //Default: Integer.MAX_VALUE (i.e. User can select as many images as he/she wants)
                .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                .setMultiSelectDoneTextColor(R.color.colorAccent) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                .disableOverSelectionMessage() //You can also decide not to show this over select message.
                .build();
        multiSelectionPicker.show(getChildFragmentManager(), "");
    }

    private boolean allPermissionsGranted() {
        return ContextCompat.checkSelfPermission(challengeFragment.getContext(), WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }

    public int getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(int challengeId) {
        this.challengeId = challengeId;
    }

    public int getPicCountChallenge() {
        return picCountChallenge;
    }

    public void setPicCountChallenge(int picCountChallenge) {
        this.picCountChallenge = picCountChallenge;
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        String formattedDate = df.format(currentTime);
        Log.d("CURRENT ", formattedDate);
        ImageDAO imageDAO = database.getImageDAO();
        for (Uri uri : uriList) {
            Image image = new Image();
            image.setImage(uri.toString());
            image.setChallengeId(getChallengeId());
            image.setSend(false);
            image.setInit_challenge(formattedDateInit);
            image.setFinish_challenge(formattedDate);

            imageDAO.insert(image);
        }
        String msg = "El Desafio se enviara cuando haya conexion";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        Log.d("onMultiImageSelected", imageDAO.getImages().toString());

        App.getInstance().setGoodConecction(true);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void getLocation(String nameFile) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        LocationDAO locationDAO = database.getLocationDAO();
        MyLocation location = new MyLocation();
        if (mLastLocation != null) {
            location.setLatitud(mLastLocation.getLatitude());
            location.setLongitud(mLastLocation.getLongitude());
            location.setNameFile(nameFile);

            locationDAO.insert(location);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
