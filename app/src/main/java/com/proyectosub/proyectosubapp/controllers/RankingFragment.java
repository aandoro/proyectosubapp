package com.proyectosub.proyectosubapp.controllers;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proyectosub.proyectosubapp.Adapters.RecycleViewItem;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.Connection;
import com.proyectosub.proyectosubapp.models.UserMobile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RankingFragment extends Fragment {

    private View rankingView;
    private List<UserMobile> places;
    private OkHttpClient client;
    private DownloadRanking downloadRanking;
    private RecyclerView recyclerView;
    private RecycleViewItem adapter;
    private RecyclerView.LayoutManager manager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rankingView = inflater.inflate(R.layout.fragment_ranking, container, false);

        init();
        downloadRanking.execute();

        return rankingView;
    }

    private void init() {
        recyclerView = rankingView.findViewById(R.id.top_list);
        manager = new LinearLayoutManager(rankingView.getContext());
        recyclerView.setLayoutManager(manager);
        client = new OkHttpClient();
        places = new ArrayList<>();
        downloadRanking = new DownloadRanking();
    }

    private void getUsers(String urlWebServices) throws IOException, JSONException {
        Request request = new Request.Builder()
                .url(urlWebServices + "user")
                .build();

        Response response = client.newCall(request).execute();
        String body = response.body().string();
        Log.d("RF-body",body);
        JSONObject jsonObject = new JSONObject(body);
        JSONArray jsonArray = jsonObject.getJSONArray("data");
        Log.d("RF", jsonArray.toString());
        for (int i = 0; i < jsonArray.length(); i++) {
            UserMobile userMobile = new UserMobile();
            userMobile.setName(jsonArray.getJSONObject(i).getString("name"));
            userMobile.setPoint(jsonArray.getJSONObject(i).getInt("point"));
            places.add(userMobile);
        }
    }

    private void firstPlaces(List<UserMobile> places) {
        Collections.sort(places, new Comparator<UserMobile>() {
            @Override
            public int compare(UserMobile userMobile, UserMobile t1) {
                return Integer.compare(userMobile.getPoint(), t1.getPoint()) * -1;
            }
        });
    }


    /**********************************************************************************************/

    public class DownloadRanking extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                getUsers(Connection.URL_WEB_SERVICES);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.loading));
            progressDialog.setMessage(getString(R.string.wait_please));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            firstPlaces(places);
            adapter = new RecycleViewItem(rankingView.getContext(), places);
            recyclerView.setAdapter(adapter);
            progressDialog.cancel();
        }
    }


}
