package com.proyectosub.proyectosubapp.controllers;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.proyectosub.proyectosubapp.Adapters.RecycleViewListGame;
import com.proyectosub.proyectosubapp.DAO.ChallengeDAO;
import com.proyectosub.proyectosubapp.DAO.TriviaDAO;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.AppDatabase;
import com.proyectosub.proyectosubapp.Utils.SharedPrefUtils;
import com.proyectosub.proyectosubapp.models.Challenge;
import com.proyectosub.proyectosubapp.models.Games;
import com.proyectosub.proyectosubapp.models.Trivia;
import com.proyectosub.proyectosubapp.models.UserMobile;
import com.proyectosub.proyectosubapp.restApi.RestApi;
import com.proyectosub.proyectosubapp.restApi.RestApiAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListGameFragment extends Fragment {

    private View listGame;
    private List<Games> games;
    private DownloadGames downloadGames;
    private RestApi restApi;
    private RecyclerView recyclerView;
    private RecycleViewListGame adapter;
    private int level;
    private ChallengeDAO challengeDAO;
    private TriviaDAO triviaDAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        listGame = inflater.inflate(R.layout.fragment_list_game, container, false);

        init();

        String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");

        if (!user.equals("null")) {
            modificarDatos();
        }

        downloadGames.execute();

        return listGame;
    }

    private void init() {
        games = new ArrayList<>();
        downloadGames = new DownloadGames();
        restApi = new RestApiAdapter().connectionEnable();
        recyclerView = listGame.findViewById(R.id.list_games);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(listGame.getContext());
        recyclerView.setLayoutManager(manager);
        AppDatabase database = Room.databaseBuilder(listGame.getContext(), AppDatabase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();
        challengeDAO = database.getChallengeDAO();
        triviaDAO = database.getTriviaDAO();
    }


    public void getChallenges() {

        Call<Challenge> call = restApi.getChallenges(this.getLevel());
        call.enqueue(new Callback<Challenge>() {
            @Override

            public void onResponse(Call<Challenge> call, Response<Challenge> response) {


                for (Challenge.Data c : response.body().data) {

                    Games game = new Games();
                    Challenge challenge = new Challenge();

                    challenge.setId(c.id);
                    challenge.setName(c.name);
                    challenge.setDescription(c.description);
                    challenge.setPoint(c.point);
                    challenge.setPicCount(c.picCount);
                    challenge.setLevelId(getLevel());

                    try {
                        challengeDAO.insert(challenge);
                    } catch (Exception e) {
                        Log.e("LGF-CHALLENGE", e.toString());
                    }

                    game.setChallenge(challenge);
                    game.setLabel("Challenge");
                    games.add(game);
                }

                getTrivias();
                Log.d("LGF-challenge", "" + games.toString());

            }

            @Override
            public void onFailure(Call<Challenge> call, Throwable t) {
                Log.d("onFailure", t.toString());

                List<Challenge> challengesDB = challengeDAO.getChallengesByLevel(getLevel());

                for (Challenge challenge : challengesDB
                ) {
                    Games game = new Games();
                    game.setChallenge(challenge);
                    game.setLabel("Challenge");
                    games.add(game);
                }
                getTrivias();
            }
        });

    }

    public void getTrivias() {
        Call<Trivia> callTwo = restApi.getTrivias(this.getLevel());
        callTwo.enqueue(new Callback<Trivia>() {
            @Override

            public void onResponse(Call<Trivia> call, Response<Trivia> response) {


                for (Trivia.Data t : response.body().data) {

                    Games game = new Games();
                    Trivia trivia = new Trivia();
                    trivia.setId(t.id);
                    trivia.setName(t.name);
                    trivia.setDescription(t.description);
                    trivia.setPoint(t.point);
                    trivia.setOptCorrect(t.optCorrect);
                    trivia.setQuestion(t.question);
                    trivia.setLevelId(getLevel());

                    try {
                        triviaDAO.insert(trivia);
                    } catch (Exception e) {
                        Log.e("LGF-TRIVIA", e.toString());
                    }

                    game.setTrivia(trivia);
                    game.setLabel("Trivia");
                    games.add(game);
                }
                Log.d("LGF-trivias", "" + games.toString());

                adapter = new RecycleViewListGame(getActivity(), listGame.getContext(), games) {
                };
                if (adapter.getItemCount() > 0) {
                    recyclerView.setAdapter(adapter);
                }

            }

            @Override
            public void onFailure(Call<Trivia> call, Throwable t) {
                Log.d("onFailure", t.toString());

                List<Trivia> triviasDB = triviaDAO.getTriviaByLevel(getLevel());

                for (Trivia trivia : triviasDB
                ) {
                    Games game = new Games();
                    game.setTrivia(trivia);
                    game.setLabel("Trivia");
                    games.add(game);
                }
                adapter = new RecycleViewListGame(getActivity(), listGame.getContext(), games) {
                };
                if (adapter.getItemCount() > 0) {
                    recyclerView.setAdapter(adapter);
                }
            }
        });

    } // fin getTrivias


    public void modificarDatos() {

        String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");
        String name = SharedPrefUtils.get(getActivity().getApplicationContext(), "name");
        String surname = SharedPrefUtils.get(getActivity().getApplicationContext(), "surname");
        String email = SharedPrefUtils.get(getActivity().getApplicationContext(), "email");
        int point = Integer.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "point"));
        int id = Integer.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "id"));


        Call<UserMobile> call = restApi.editProfile(id, name, surname, user, email, point);
        call.enqueue(new Callback<UserMobile>() {
            @Override

            public void onResponse(Call<UserMobile> call, Response<UserMobile> response) {
                Log.d("actualiza los puntos:", String.valueOf(response.body().getData().point));
            }

            @Override
            public void onFailure(Call<UserMobile> call, Throwable t) {
                Log.d("onFailure", t.toString());

            }
        });


    } // fin del modificar datos


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    /**********************************************************************************************/

    public class DownloadGames extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... voids) {

            getChallenges();

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.loading));
            progressDialog.setMessage(getString(R.string.wait_please));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Log.d("LGF-onpostExe", "QUE HAY" + games.toString() + " - " + games.size());
            progressDialog.cancel();
        }
    }

}
