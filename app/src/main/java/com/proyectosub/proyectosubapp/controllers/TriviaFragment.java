package com.proyectosub.proyectosubapp.controllers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.proyectosub.proyectosubapp.Adapters.RecycleViewOptionTrivia;
import com.proyectosub.proyectosubapp.DAO.OptionDAO;
import com.proyectosub.proyectosubapp.DAO.TriviaDAO;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.AppDatabase;
import com.proyectosub.proyectosubapp.Utils.Connection;
import com.proyectosub.proyectosubapp.Utils.SharedPrefUtils;
import com.proyectosub.proyectosubapp.models.Option;
import com.proyectosub.proyectosubapp.models.Trivia;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class TriviaFragment extends Fragment {

    private View triviaFragment;
    private List<Option> options;
    private List<RadioButton> radioButtonList;
    private int triviaId;
    private int ptsTrivia;
    private DownloadTrivia downloadTrivia;
    private DownloadOptions downloadOptions;
    private OkHttpClient client;
    private Trivia trivia;
    private TextView questionT;
    private Gson gson;
    private FloatingActionButton enviar;
    private RecyclerView recyclerView;
    private RadioGroup radioGroup;
    private RelativeLayout relativeLayout;
    private RecycleViewOptionTrivia adapter;
    private String optionSelected;
    private TriviaDAO triviaDAO;
    private OptionDAO optionDAO;


    private Button.OnClickListener bOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(triviaFragment.getContext());
            builder.setTitle(R.string.answerGame);
            if (trivia.getOptCorrect().equals(optionSelected)) {

                String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");

                if (user.equals("null")) {

                    builder.setMessage(R.string.answerOKGameNoUser);
                    builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getFragmentManager().popBackStack();

                        }
                    });

                } else {
                    builder.setMessage(R.string.answerOKGame);
                    builder.setPositiveButton(R.string.takePoints, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            puntuarUsuario();
                            getFragmentManager().popBackStack();
                        }
                    });
                }
            } else {


                String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");

                if (user.equals("null")) {

                    builder.setMessage(R.string.answerIncorrectGameNoUser);
                    builder.setPositiveButton(R.string.noTryAgain, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getFragmentManager().popBackStack();

                        }
                    });

                } else {
                    builder.setMessage(R.string.answerIncorrectGame);
                    builder.setPositiveButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            quitarPuntos();
                        }
                    });

                    builder.setNegativeButton(R.string.noTryAgain, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getFragmentManager().popBackStack();
                        }
                    });
                }
            }
            builder.show();
        }
    };

    private RadioGroup.OnCheckedChangeListener selectButton = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            RadioButton radioButton = triviaFragment.findViewById(i);
            optionSelected = (String) radioButton.getText();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        triviaFragment = inflater.inflate(R.layout.fragment_trivia, container, false);

        init();

        downloadTrivia.execute();

        radioGroup.setOnCheckedChangeListener(selectButton);
        enviar.setOnClickListener(bOnClickListener);

        return triviaFragment;
    }

    private void init() {

        options = new ArrayList<>();
        radioButtonList = new ArrayList<>();
        downloadTrivia = new DownloadTrivia();
        downloadOptions = new DownloadOptions();
        client = new OkHttpClient();
        trivia = new Trivia();
        questionT = (TextView) triviaFragment.findViewById(R.id.questionTrivia);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
        enviar = triviaFragment.findViewById(R.id.btn_send);
        radioGroup = new RadioGroup(triviaFragment.getContext());
        relativeLayout = triviaFragment.findViewById(R.id.optTrivia);
        AppDatabase database = Room.databaseBuilder(triviaFragment.getContext(), AppDatabase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();

        triviaDAO = database.getTriviaDAO();
        optionDAO = database.getOptionDAO();

    }

    private void getTrivia(String urlWebServices) throws JSONException, IOException {
        Request request = new Request.Builder()
                .url(urlWebServices + "trivias" + "/" + triviaId)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
        }
        String body = response.body().string();
        JSONObject jsonObject = new JSONObject(body);
        trivia = gson.fromJson(jsonObject.getJSONObject("data").toString(), Trivia.class);


        ptsTrivia = trivia.getPoint();
        Log.d("PTS total de la Trivia", String.valueOf(ptsTrivia));

    }


    private void getOptions(String urlWebServices) throws IOException, JSONException {
        Option option;
        RadioButton radioButton;
        Request request = new Request.Builder()
                .url(urlWebServices + "options/" + triviaId)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
        }
        String body = response.body().string();
        JSONArray jsonArray = new JSONArray(body);
        Log.d("TF-getOptions", jsonArray.toString());
        for (int i = 0; i < jsonArray.length(); i++) {
            Option optionTemp = new Option();
            optionTemp.setOption(jsonArray.getJSONObject(i).getString("option"));
            optionTemp.setTriviaId(triviaId);
            try {
                optionDAO.insert(optionTemp);
            } catch (Exception e) {
                Log.e("SPECIEFRAGMENT", e.toString());
            }
            radioButton = new RadioButton(triviaFragment.getContext());
            radioButton.setText(jsonArray.getJSONObject(i).getString("option"));
            radioButtonList.add(radioButton);
        }


    }

    public void puntuarUsuario() {

        String puntosActualizados = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");
        int pts = Integer.valueOf(puntosActualizados);
        pts = pts + ptsTrivia;

        SharedPrefUtils.put(getActivity().getApplicationContext(), "point", String.valueOf(pts));

        Toast toast = Toast.makeText(getActivity().getApplicationContext(), R.string.pointsReceived, Toast.LENGTH_SHORT);
        toast.show();


    }

    private void quitarPuntos() {

        int ptsRestar = trivia.getPoint() / radioButtonList.size();
        Log.d("Puntos a restar: ", String.valueOf(ptsRestar));

        if (ptsTrivia < ptsRestar) {
            ptsTrivia = 0;
        } else {
            ptsTrivia = ptsTrivia - ptsRestar;

        }

    }


    public int getTriviaId() {
        return triviaId;
    }

    public void setTriviaId(int triviaId) {
        this.triviaId = triviaId;
    }

    /**********************************************************************************************/

    public class DownloadTrivia extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                getTrivia(Connection.URL_WEB_SERVICES);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                trivia = triviaDAO.getTriviaById(triviaId);
                ptsTrivia = trivia.getPoint();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.loading));
            progressDialog.setMessage(getString(R.string.wait_please));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            downloadOptions.execute();
            progressDialog.cancel();
        }
    }

    /**********************************************************************************************/

    public class DownloadOptions extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                getOptions(Connection.URL_WEB_SERVICES);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                options = optionDAO.getOptionsByTrivia(triviaId);
                for (Option option : options
                ) {
                    RadioButton radioButton = new RadioButton(triviaFragment.getContext());
                    radioButton.setText(option.getOption());
                    radioButtonList.add(radioButton);
                }
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.loading));
            progressDialog.setMessage(getString(R.string.wait_please));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            questionT.setText(trivia.getQuestion());
            /*adapter = new RecycleViewOptionTrivia(triviaFragment.getContext(), options);
            recyclerView.setAdapter(adapter);*/
            for (RadioButton rb : radioButtonList) {
                radioGroup.addView(rb);
            }
            relativeLayout.addView(radioGroup);
            progressDialog.cancel();
        }
    }

}
