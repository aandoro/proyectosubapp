package com.proyectosub.proyectosubapp.controllers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.proyectosub.proyectosubapp.MainActivity;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.SharedPrefUtils;
import com.proyectosub.proyectosubapp.models.UserMobile;
import com.proyectosub.proyectosubapp.restApi.RestApi;
import com.proyectosub.proyectosubapp.restApi.RestApiAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private static final String KEY_STATUS = "status";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_NAME = "name";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_EMPTY = "";

    private Button btnLogin;
    private Button btnRegister;

    private EditText etName;
    private EditText etSurname;
    private EditText etUsername;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etConfirmPassword;

    private  ProgressDialog pDialog;



    private String name;
    private String surname;
    private String username;
    private String email;
    private String password;
    private String confirmPassword;
    private int role;


    private RestApi restApi;
 //   private AsyncRegister asyncRegister;

  //  private OkHttpClient client;


    private Button.OnClickListener clickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    };

    private Button.OnClickListener clickRegister = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            name = etName.getText().toString().toLowerCase().trim() ;
            surname = etSurname.getText().toString().toLowerCase().trim();
            username = etUsername.getText().toString().toLowerCase().trim();
            email = etEmail.getText().toString().trim();
            password = etPassword.getText().toString().trim();
            confirmPassword = etConfirmPassword.getText().toString().trim();
            role = 3;

            if (validateInputs()) {

              //  asyncRegister.execute();
                displayLoader();
                registerUser();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();

        btnLogin.setOnClickListener(clickLogin);
        btnRegister.setOnClickListener(clickRegister);

    }

    private void init() {

       // asyncRegister = new AsyncRegister();
     //   client = new OkHttpClient();
        btnLogin = findViewById(R.id.btnRegisterLogin);
        btnRegister = findViewById(R.id.btnRegister);

        etName = findViewById(R.id.etName);
        etSurname = findViewById(R.id.etSurname);
        etUsername = findViewById(R.id.etUsername);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);

        pDialog = new ProgressDialog(RegisterActivity.this);



        restApi = new RestApiAdapter().connectionEnable();
    }

    private boolean validateInputs() {

        if (KEY_EMPTY.equals(name)) {
            etName.setError(getResources().getString(R.string.name_empty));
            etName.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(surname)) {
            etSurname.setError(getResources().getString(R.string.surname_empty));
            etSurname.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(username)) {
            etUsername.setError(getResources().getString(R.string.username_empty));
            etUsername.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(email)) {
            etEmail.setError(getResources().getString(R.string.email_empty));
            etEmail.requestFocus();
            return false;

        }
        if (KEY_EMPTY.equals(password)) {
            etPassword.setError(getResources().getString(R.string.password_empty));
            etPassword.requestFocus();
            return false;
        }

        if (KEY_EMPTY.equals(confirmPassword)) {
            etConfirmPassword.setError(getResources().getString(R.string.passwordC_empty));
            etConfirmPassword.requestFocus();
            return false;
        }
        if (!password.equals(confirmPassword)) {
            etConfirmPassword.setError(getResources().getString(R.string.password_confirm));
            etConfirmPassword.requestFocus();
            return false;
        }

        return true;
    }


    private void registerUser() {
      //  displayLoader();
        /**
        RequestBody requestBody = new MultipartBody.Builder().addFormDataPart(KEY_NAME, name).addFormDataPart(KEY_PASSWORD, password).addFormDataPart(KEY_EMAIL, email).build();
        Request request = new Request.Builder().url(Connection.URL_WEB_SERVICES + "register").post(requestBody).build();
        try {
            Response response = client.newCall(request).execute();
            Log.d("RA-register", response.body().toString());
            loadDashboard();
        } catch (IOException e) {
            e.printStackTrace();
        }

        */


        Call<UserMobile> call = restApi.register(name, surname, username, email, password, confirmPassword, role);
        call.enqueue(new Callback<UserMobile>() {
            @Override
            public void onResponse(Call<UserMobile> call, Response<UserMobile> response) {

                onCancelled();
                Log.d("onResponse",String.valueOf(response.code()));
                Log.d("onResponseERROR",response.message());

                if (response.code() == 200){

                    SharedPrefUtils.put(getApplicationContext(), "username", username);
                    SharedPrefUtils.put(getApplicationContext(), "password", password);

                    Toast toast = Toast.makeText(getApplicationContext(), "Te registraste :D", Toast.LENGTH_SHORT);


                   Log.d("RAW",String.valueOf(response.raw()));
                    Log.d("BODY",String.valueOf(response.body()));
                    Log.d("TO-STRING",response.toString());

                    traerDatos();

                    loadDashboard();
                    toast.show();
                }
                else{
                    Toast toast = Toast.makeText(getApplicationContext(), "ver error", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }

            @Override
            public void onFailure(Call<UserMobile> call, Throwable t) {
                Log.d("onFailure",t.toString());
                Toast toast = Toast.makeText(getApplicationContext(), "Usuario y contraseña incorrecta", Toast.LENGTH_SHORT);
                toast.show();
            }
        });



    }  // FIN REGISTERUSER

    private void displayLoader() {
       // ProgressDialog pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage(getResources().getString(R.string.singning_up));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void onCancelled() {
        pDialog.cancel();

    }

    public void traerDatos(){

        String user = SharedPrefUtils.get(getApplicationContext(), "username");
        Log.d("USERNAME:", user);



        Call<UserMobile> call = restApi.dataUser( user);
        call.enqueue(new Callback<UserMobile>() {
            @Override

            public void onResponse(Call<UserMobile> call, Response<UserMobile> response) {


                UserMobile d = response.body();

                SharedPrefUtils.put(getApplicationContext(), "id", d.data.id);
                SharedPrefUtils.put(getApplicationContext(), "name", d.data.name);
                SharedPrefUtils.put(getApplicationContext(), "surname", d.data.surname);
                SharedPrefUtils.put(getApplicationContext(), "email", d.data.email);


                SharedPrefUtils.put(getApplicationContext(), "point", String.valueOf(d.data.point));


            }

            @Override
            public void onFailure(Call<UserMobile> call, Throwable t) {
                Log.d("onFailure",t.toString());
                Toast toast = Toast.makeText(getApplicationContext(), "error al traer info", Toast.LENGTH_SHORT);
                toast.show();
            }
        });


    } // fin del traer datos

    private void loadDashboard() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();

    }


    /******************************************************************************/

    /**
    public class AsyncRegister extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialog;

        @Override
        protected Void doInBackground(Void... voids) {

            registerUser();
            return null;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(RegisterActivity.this);
            pDialog.setMessage("Signing Up.. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pDialog.cancel();
        }

        @Override
        protected void onCancelled() {
            cancel(true);
            Toast toast = Toast.makeText(getApplicationContext(), "Cancelado.", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    */

} // FIN DE LA CLASE
