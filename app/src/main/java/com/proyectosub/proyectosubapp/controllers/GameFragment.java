package com.proyectosub.proyectosubapp.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.proyectosub.proyectosubapp.DAO.GroupDAO;
import com.proyectosub.proyectosubapp.DAO.LevelDAO;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.AppDatabase;
import com.proyectosub.proyectosubapp.Utils.SharedPrefUtils;
import com.proyectosub.proyectosubapp.models.Level;
import com.proyectosub.proyectosubapp.restApi.RestApi;
import com.proyectosub.proyectosubapp.restApi.RestApiAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import okhttp3.Response;


public class GameFragment extends Fragment {

    private LevelAdapter levelAdapter;
    private SharedPreferences preferences;
    private List<Level> leveles;
    private GridView gridView;
    private TextView points;
    private FloatingActionButton rankingButton;
    private View gameView;
    private AppDatabase database;
    private LevelDAO levelDAO;

    private RestApi restApi;
   // private OkHttpClient client;

    private DownloadLevels downloadLevels;
    private ProgressDialog progressDialog;

    private Button.OnClickListener bOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            rankingList();
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        gameView = inflater.inflate(R.layout.game_fragment, container, false);

        init(gameView);

        getLeveles();
      //  downloadLevels.execute();

        rankingButton.setOnClickListener(bOnClickListener);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                int position = i;

                if(position == 0){
                    preferences.edit().putString( "done" + position, "yes").apply();
                }

                if (preferences.getString("done" + position, null) != null && preferences.getString("done" + position, null).equals("yes")){
                    view.setClickable(true);
                    view.setEnabled(true);
                    listGames(i);
                }else{


                    view.setClickable(false);
                    view.setEnabled(false);
                }


            }
        });

        return gameView;
    }

    private void init(View gameView) {

        restApi = new RestApiAdapter().connectionEnable();
       // client = new OkHttpClient();


        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        levelAdapter = new LevelAdapter();
        leveles = new ArrayList<>();
        gridView = gameView.findViewById(R.id.gridview);
        rankingButton = gameView.findViewById(R.id.button_Ranking);
        downloadLevels = new DownloadLevels();
        points =(TextView) gameView.findViewById(R.id.pointsUser);

        String ptsUser = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");

        if (ptsUser.equals("null")){
            points.append(": 0 ");
        }else{
            points.append(": "+ ptsUser);
        }

        database = Room.databaseBuilder(gameView.getContext(), AppDatabase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();
        levelDAO = database.getLevelDAO();


    }// fin init

    public void listGames(int position) {
        Fragment fragment = new ListGameFragment();
        ((ListGameFragment) fragment).setLevel(position + 1);
        replaceFragment(fragment);
    }

    public void rankingList() {
        Fragment fragment = new RankingFragment();
        replaceFragment(fragment);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

  /**  public void getLeveles(String url) throws IOException, JSONException {
        Request request = new Request.Builder()
                .url(url + "leveles")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
        }
        String body = response.body().string();
        JSONObject jsonObject = new JSONObject(body);
        JSONArray jsonArray = jsonObject.getJSONArray("data");
        for (int i = 0; i < jsonArray.length(); i++) {
            Level level = new Level();
            level.setId(jsonArray.getJSONObject(i).getInt("id"));
            level.setName(jsonArray.getJSONObject(i).getString("name"));
            level.setNumber(jsonArray.getJSONObject(i).getInt("number"));
            level.setScoreRequired(jsonArray.getJSONObject(i).getInt("scoreRequired"));

            leveles.add(level);
            Log.d("GF", "" + leveles.get(i));


        }

        } // fin getLeveles  */


  public void getLeveles()  {


      final ProgressDialog dialog = ProgressDialog.show(getActivity(), getString(R.string.loading), getString(R.string.wait_please), true);

      Call<Level> callTwo = restApi.getLeveles();
      callTwo.enqueue(new Callback<Level>() {
          @Override

          public void onResponse(Call<Level> call, Response<Level> response) {


              for (Level.Data l: response.body().data) {
                  Level level = new Level();
                  level.setId(l.id);
                  level.setName(l.name);
                  level.setNumber(l.number);
                  level.setScoreRequired(l.scoreRequired);

                  try {
                      levelDAO.insert(level);
                  } catch (Exception e) {
                      Log.d("GameFragment", e.toString());
                  }

                  leveles.add(level);

              }
              Log.d("GF", "" + leveles.toString());
              setearAdapter();

              dialog.cancel();

          }

          @Override
          public void onFailure(Call<Level> call, Throwable t) {

           //   dialog.setTitle("Error");
           //   dialog.setMessage("Se produjo un error");
           //   dialog.setCancelable(true);

              leveles = levelDAO.getLevels();
              setearAdapter();
              dialog.cancel();

              Log.d("onFailure",t.toString());

          }
      });


  } // fin getLeveles


    public void setearAdapter(){


        if (levelAdapter.getCount() > 0) {
            gridView.setAdapter(levelAdapter);
        }

    }


    /**********************************************************************************************/



    public class DownloadLevels extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            getLeveles();
            return null;
        }

        @Override
        protected void onPreExecute() {
        /*    super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
           progressDialog.setTitle(getString(R.string.loading));
            progressDialog.setMessage(getString(R.string.wait_please));
            progressDialog.setCancelable(false);
            progressDialog.show(); */
        }

        @Override
        protected void onPostExecute(Void aVoid) {

          //            if (levelAdapter.getCount() > 0) {
                //     gridView.setAdapter(levelAdapter);
                //  }
          /*  setearAdapter();
            progressDialog.cancel(); */
        }
    }

    /**********************************************************************************************/

    public class LevelAdapter extends BaseAdapter {

        public List<Integer> mProgressIds;
        private LayoutInflater mInflater;

        public LevelAdapter() {
            mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return leveles.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {


            ViewHolder holder;
            mProgressIds = new ArrayList<Integer>();
            Float percentage = 0.f;

            for (int i = 0; i < leveles.size(); i++) {

                int progress = leveles.get(i).getScoreRequired();
                mProgressIds.add(progress);
            }

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(
                        R.layout.item_leveles, null);
                holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress);
                holder.levelNumberTextView = (TextView) convertView.findViewById(R.id.levelItem);
                holder.scoreTextView = (TextView) convertView.findViewById(R.id.score);
                holder.lockImageView = (ImageView) convertView.findViewById(R.id.lock_imageView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }



            // CALCULA EL PORCENTAJE
            int max = leveles.get(position).getScoreRequired();

            String points = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");
            int pointUser = 0;

            if (points.equals("null")){
                pointUser = 0;
                percentage = 0.f;
            }else{
                percentage = (Float.valueOf(points) / max) *100 ;
                pointUser = Integer.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "point"));

            }



            Log.d("POSITION",  String.valueOf(position));
            preferences.edit().putString( "done" + position, null).apply();


            if (position > 0) {
                //detect if level opened


                // ARREGLO PARA EL PORCENTAJE EN NEGATIVO

                int ptsAccRequeridos = 0;
                for (int i = 0; i < position; i++) {
                    ptsAccRequeridos = ptsAccRequeridos + mProgressIds.get(i);
                }

                Log.d("valor acc", String.valueOf(ptsAccRequeridos));

                String p = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");

                int ptsUser = 0;

                if (p.equals("null")){
                    ptsUser = 0;
                    if (ptsUser >= ptsAccRequeridos){
                        preferences.edit().putString("done" + position, "yes").apply();
                    }
                }else{
                    ptsUser = Integer.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "point"));

                    if (ptsUser >= ptsAccRequeridos){
                        preferences.edit().putString("done" + position, "yes").apply();
                    }

                }

            //    if (pointUser >= ptsAccRequeridos){
            //        preferences.edit().putString("done" + position, "yes").apply();
            //    }


                // HASTA ACA



                if (preferences.getString("done" + position, null) != null && preferences.getString("done" + position, null).equals("yes")) {

                    holder.levelNumberTextView.setText(position + 1 + "");
                    holder.levelNumberTextView.setBackgroundResource(R.drawable.circle);
                    holder.lockImageView.setVisibility(View.GONE);

                    int acc = 0;
                    for (int i = 0; i < position; i++) {
                        acc = acc + mProgressIds.get(i);
                    }

                    Log.d("valor acc", String.valueOf(acc));

                    percentage = ((Float.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "point")) - acc) / max) * 100;

                    // desde aca
                    holder.progressBar.setVisibility(View.VISIBLE);
                    holder.scoreTextView.setVisibility(View.VISIBLE);

                    //AGREGADO
                    Log.d("VALOR ACTUAL PORC:", String.valueOf(percentage));

                    String progress = String.format(Locale.ENGLISH, "%.0f", percentage);


                    if (Integer.valueOf(progress) > holder.progressBar.getMax()){
                        progress = String.format(Locale.ENGLISH, "%.0f", Float.valueOf(holder.progressBar.getMax()));
                        holder.progressBar.setProgress(Integer.valueOf(progress));
                    }else {
                        holder.progressBar.setProgress(Integer.valueOf(progress));
                    }

                    holder.levelNumberTextView.setText(position + 1 + "");
                    holder.scoreTextView.setText(progress + "%");


                    if (pointUser >= acc){
                        preferences.edit().putString("done" + position,"yes").apply();
                    }


                } else {
                    holder.levelNumberTextView.setText("");
                    holder.progressBar.setVisibility(View.INVISIBLE);
                    holder.scoreTextView.setVisibility(View.INVISIBLE);
                    holder.lockImageView.setVisibility(View.VISIBLE);
                    holder.levelNumberTextView.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                }

            } else {
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.scoreTextView.setVisibility(View.VISIBLE);

                //AGREGADO
                String progress = String.format(Locale.ENGLISH, "%.0f", percentage);


                if (Integer.valueOf(progress) > holder.progressBar.getMax()){
                    progress = String.format(Locale.ENGLISH, "%.0f", Float.valueOf(holder.progressBar.getMax()));
                    holder.progressBar.setProgress(Integer.valueOf(progress));
                }else {
                    holder.progressBar.setProgress(Integer.valueOf(progress));
                }

                holder.levelNumberTextView.setText(position + 1 + "");
                holder.scoreTextView.setText(progress + "%");


                if (pointUser >= mProgressIds.get(position)){
                    preferences.edit().putString("done" + position,"yes").apply();
                }

            }
            return convertView;
        }

    }

    class ViewHolder {
        TextView levelNumberTextView, scoreTextView;
        ProgressBar progressBar;
        ImageView lockImageView;
    }

}
