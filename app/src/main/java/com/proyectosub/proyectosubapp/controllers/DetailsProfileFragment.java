package com.proyectosub.proyectosubapp.controllers;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.SharedPrefUtils;
import com.proyectosub.proyectosubapp.models.UserMobile;
import com.proyectosub.proyectosubapp.restApi.RestApi;
import com.proyectosub.proyectosubapp.restApi.RestApiAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailsProfileFragment extends Fragment {

    private View viewDetailsProfile;
    private FloatingActionButton configurationButton;
    private TextView txtName;
    private TextView txtSurname;
    private TextView txtUsername;
    private TextView txtEmail;
    private TextView txtPts;
    private TextView txtNameEdit;
    private TextView txtSurnameEdit;
    private TextView txtUsernameEdit;
    private TextView txtEmailEdit;
    private TextView txtPtsEdit;
    private TextView txtChangePhoto;

    private Button editProfile;
    private Button saveProfile;
    private Button cancelEditProfile;
    private RestApi restApi;

    private UserMobile datos;


    private DownloadData downloadData;


    private Button.OnClickListener bOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            configuration();
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        viewDetailsProfile = inflater.inflate(R.layout.details_profile, container, false);



            init();
            downloadData.execute();

        configurationButton.setOnClickListener(bOnClickListener);

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                activarEdit();

            }
        });

        cancelEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cancelEdit();

            }
        });

        saveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modificarDatos();

            }
        });



        return viewDetailsProfile;
    }

    private void init() {

        restApi = new RestApiAdapter().connectionEnable();
        downloadData = new DownloadData();

        txtName = viewDetailsProfile.findViewById(R.id.textNameUser);
        txtSurname = viewDetailsProfile.findViewById(R.id.textSurnameUser);
        txtUsername = viewDetailsProfile.findViewById(R.id.textUsernameUser);
        txtEmail = viewDetailsProfile.findViewById(R.id.textEmailUser);

        txtNameEdit = viewDetailsProfile.findViewById(R.id.textNameUserEdit);
        txtSurnameEdit = viewDetailsProfile.findViewById(R.id.textSurnameUserEdit);
        txtUsernameEdit = viewDetailsProfile.findViewById(R.id.textUsernameUserEdit);
        txtEmailEdit = viewDetailsProfile.findViewById(R.id.textEmailUserEdit);
        txtPtsEdit = viewDetailsProfile.findViewById(R.id.textPointsUserEdit);


        txtPts = viewDetailsProfile.findViewById(R.id.textPointsUser);
        configurationButton = viewDetailsProfile.findViewById(R.id.button_Configuration);

        editProfile = viewDetailsProfile.findViewById(R.id.buttonEdit);
        saveProfile = viewDetailsProfile.findViewById(R.id.buttonSave);
        cancelEditProfile= viewDetailsProfile.findViewById(R.id.buttonCancel);



    //    txtChangePhoto = viewDetailsProfile.findViewById(R.id.changePhotoProfile);

        datos = new UserMobile();

        cancelEdit();

    }

    public void configuration() {
        Fragment fragment = new ProfileFragment();
        replaceFragment(fragment);
    }






    public void activarEdit(){

        editProfile.setVisibility(View.INVISIBLE);
      //  txtChangePhoto.setVisibility(View.VISIBLE);
        saveProfile.setVisibility(View.VISIBLE);
        cancelEditProfile.setVisibility(View.VISIBLE);


        txtNameEdit.setFocusable(true);
        txtNameEdit.setEnabled(true);

        txtSurnameEdit.setFocusable(true);
        txtSurnameEdit.setEnabled(true);

    //    txtUsernameEdit.setFocusable(true);
     //   txtUsernameEdit.setEnabled(true);

        txtEmailEdit.setFocusable(true);
        txtEmailEdit.setEnabled(true);

    }
    public void cancelEdit(){

        editProfile.setVisibility(View.VISIBLE);
//        txtChangePhoto.setVisibility(View.INVISIBLE);
        saveProfile.setVisibility(View.INVISIBLE);
        cancelEditProfile.setVisibility(View.INVISIBLE);

        txtNameEdit.setEnabled(false);
        txtSurnameEdit.setEnabled(false);
        txtUsernameEdit.setEnabled(false);
        txtEmailEdit.setEnabled(false);
        txtPtsEdit.setEnabled(false);

        txtNameEdit.setText(datos.getName());
        txtSurnameEdit.setText(datos.getSurname());
    //    txtUsernameEdit.setText(datos.getUsername());
        txtEmailEdit.setText(datos.getEmail());

    }

    public void traerDatos(){

        String username = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");
        Log.d("USERNAME:", username);
        String name = SharedPrefUtils.get(getActivity().getApplicationContext(), "name");
        String surname = SharedPrefUtils.get(getActivity().getApplicationContext(), "surname");
        String email = SharedPrefUtils.get(getActivity().getApplicationContext(), "email");
        String point = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");
        String id = SharedPrefUtils.get(getActivity().getApplicationContext(), "id");



        txtNameEdit.setText(name);
        txtSurnameEdit.setText(surname);
        txtUsernameEdit.setText(username);
        txtEmailEdit.setText(email);
        txtPtsEdit.setText(String.valueOf(point));

        Log.d("ID USER", id);

        datos.setId(Integer.valueOf(id));
        datos.setName(name);
        datos.setSurname(surname);
        datos.setUsername(username);
        datos.setEmail(email);
        datos.setPoint(Integer.valueOf(point));



    }





    public void modificarDatos(){

        String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");
        Log.d("USERNAME:", user);


        String name =  txtNameEdit.getText().toString();
        String surname =  txtSurnameEdit.getText().toString();
        String email = txtEmailEdit.getText().toString();


        SharedPrefUtils.put(getActivity().getApplicationContext(), "name", name);
        SharedPrefUtils.put(getActivity().getApplicationContext(), "surname", surname);
        SharedPrefUtils.put(getActivity().getApplicationContext(), "email", email);



        datos.setName(name);
        datos.setSurname(surname);
        datos.setEmail(email);
        int point = datos.getPoint();
        int id = datos.getId();


        Call<UserMobile> call = restApi.editProfile(id, name, surname, user, email, point);
        call.enqueue(new Callback<UserMobile>() {
            @Override

            public void onResponse(Call<UserMobile> call, Response<UserMobile> response) {



                UserMobile d = response.body();

                Log.d("QUE HAY 2: ", d.data.toString());


                txtNameEdit.setText(d.data.name);
                txtSurnameEdit.setText(d.data.surname);
                txtEmailEdit.setText(d.data.email);
                txtPtsEdit.setText(String.valueOf(d.data.point));



                cancelEdit();

                Toast toast = Toast.makeText(getActivity().getApplicationContext(),R.string.details_editOK, Toast.LENGTH_SHORT);
                toast.show();

            }

            @Override
            public void onFailure(Call<UserMobile> call, Throwable t) {
                Log.d("onFailure",t.toString());
                Toast toast = Toast.makeText(getActivity().getApplicationContext(), "No Hay conexión", Toast.LENGTH_SHORT);
                toast.show();
            }
        });


    } // fin del modificar datos


    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }



    /**********************************************************************************************/

    public class DownloadData extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... voids) {

           traerDatos();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.loading));
            progressDialog.setMessage(getString(R.string.wait_please));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog.cancel();
        }
    }






} // FIN DE LA CLASE


