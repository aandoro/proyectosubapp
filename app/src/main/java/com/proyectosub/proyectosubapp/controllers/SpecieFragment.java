package com.proyectosub.proyectosubapp.controllers;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.proyectosub.proyectosubapp.DAO.GroupDAO;
import com.proyectosub.proyectosubapp.DAO.SpecieDAO;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.AppDatabase;
import com.proyectosub.proyectosubapp.Utils.Connection;
import com.proyectosub.proyectosubapp.models.Group;
import com.proyectosub.proyectosubapp.models.Specie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class SpecieFragment extends Fragment {

    private View infoGroup;
    private List<Specie> species;
    private Group group;
    private int groupId;
    private DownloadGroup downloadGroup;
    private DownloadSpecies downloadSpecies;
    private DownloadImageSpecie downloadImageSpecie;
    private TextView nameG;
    private TextView descriptionG;

    private ProgressDialog progressDialog;


    private OkHttpClient client;
    private Gson gson;

    private SpecieAdapter specieAdapter;

    private GridView gridView;

    private SpecieDAO specieDao;
    private GroupDAO groupDao;
    private List<Specie> speciesdb;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        infoGroup = inflater.inflate(R.layout.fragment_specie, container, false);

        init();

        downloadGroup.execute();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                detailsSpecie(i);
            }
        });


        return infoGroup;

    }

    @SuppressLint("WrongConstant")
    private void init() {
        species = new ArrayList<>();
        downloadGroup = new DownloadGroup();

        specieAdapter = new SpecieAdapter();

        gridView = infoGroup.findViewById(R.id.gridview);

        downloadSpecies = new DownloadSpecies();
        downloadImageSpecie = new DownloadImageSpecie();

        client = new OkHttpClient();
        group = new Group();
        nameG = (TextView) infoGroup.findViewById(R.id.nameGroup);
        descriptionG = (TextView) infoGroup.findViewById(R.id.descriptionGroup);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
        AppDatabase database = Room.databaseBuilder(infoGroup.getContext(), AppDatabase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();
        specieDao = database.getSpecieDAO();
        groupDao = database.getGroupDAO();

    }


    public void detailsSpecie(int position) {
        Fragment fragment = new DetailsSpecieFragment();
        ((DetailsSpecieFragment) fragment).setSpecie(species.get(position));
        replaceFragment(fragment);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    private void getGroup() throws JSONException {
        Request request = new Request.Builder()
                .url(Connection.URL_WEB_SERVICES + "groups" + "/" + groupId)
                .build();

        Response response = null;
        String body = null;
        JSONObject jsonObject = null;
        try {
            response = client.newCall(request).execute();
            body = response.body().string();
            jsonObject = new JSONObject(body);
            group = gson.fromJson(jsonObject.getJSONObject("data").toString(), Group.class);
        } catch (IOException e) {
            group = groupDao.getGroupById((long) groupId);
        }
        nameG.setText(group.getName());
        descriptionG.setText(group.getDescription());
    }


    private void getSpecies() throws JSONException {


        Specie specie;
        Request request = new Request.Builder()
                .url(Connection.URL_WEB_SERVICES + "species/group/" + groupId)
                .build();

        Response response = null;
        String body = null;
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;

        try {
            response = client.newCall(request).execute();
            body = response.body().string();
            jsonObject = new JSONObject(body);
            jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {

                specie = new Specie();
                specie.setId(jsonArray.getJSONObject(i).getInt("id"));
                specie.setName(jsonArray.getJSONObject(i).getString("name"));
                specie.setScientificName(jsonArray.getJSONObject(i).getString("scientificName"));
                specie.setDescription(jsonArray.getJSONObject(i).getString("description"));
                specie.setDiet(jsonArray.getJSONObject(i).getString("diet"));
                specie.setHabitat(jsonArray.getJSONObject(i).getString("habitat"));
                specie.setReproduction(jsonArray.getJSONObject(i).getString("reproduction"));
                specie.setGroupId(groupId);
                try {
                    specieDao.insert(specie);
                } catch (Exception e) {
                    Log.e("SPECIEFRAGMENT", e.toString());
                }

                species.add(specie);
                Log.i("SPECIE trivia: ", specie.toString());

            }
        } catch (IOException e) {
            e.printStackTrace();
            species = specieDao.getSpecieByGroup(groupId);
        }


    }


    private void getImageSpecie() throws IOException, JSONException {

        String nameImage = "";

        int idSpecie = 0;
        URL imageUrl = null;

        for (Specie specie : species) {
            idSpecie = specie.getId();

            Request request = new Request.Builder()
                    .url(Connection.URL_WEB_SERVICES + "imagespecies/specie/" + idSpecie)
                    .build();

            Response response = client.newCall(request).execute();
            Log.d("URL", Connection.URL_WEB_SERVICES + "imagespecies/specie/" + idSpecie);
            String body = response.body().string();
            Log.d("SF: ", body);
            JSONObject jsonObject = new JSONObject(body);
            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {

                nameImage = jsonArray.getJSONObject(i).getString("image");

                Log.i("name image: ", nameImage);

                try {
                    imageUrl = new URL(Connection.URL_IMAGE_SPECIE + nameImage);
                    HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
                    conn.connect();
                    Bitmap loadedImage = BitmapFactory.decodeStream(conn.getInputStream());

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    loadedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    specie.setImage(byteArray);
                    loadedImage.recycle();
                    specieDao.update(specie);
                } catch (IOException e) {

                    e.printStackTrace();

                }

            }

        }


    }


    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    /**********************************************************************************************/

    public class DownloadGroup extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                getGroup();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.loading));
            progressDialog.setMessage(getString(R.string.wait_please));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            downloadSpecies.execute();
        }
    }

    /**********************************************************************************************/

    public class DownloadSpecies extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            try {
                getSpecies();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            downloadImageSpecie.execute();
        }
    }

    /**********************************************************************************************/

    public class SpecieAdapter extends BaseAdapter {


        private LayoutInflater mInflater;


        private Specie specie;

        SpecieAdapter() {
            mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        public int getCount() {
            return species.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {

           // species = specieDao.getSpecieByGroup(groupId);
            specie = species.get(position);
            ViewHolder holder;

            Log.d("ESPECIE DONDE SE ROMPE", specie.toString());

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.shr_specie_card, null);

                holder.tittle = (TextView) convertView.findViewById(R.id.titleSpecie);

                holder.subtittle = (TextView) convertView.findViewById(R.id.subtitleSpecie);
                holder.specieImage = (ImageView) convertView.findViewById(R.id.imageSpecie);

                holder.tittle.setText(specie.getName());
                holder.subtittle.setText(specie.getScientificName());
                Bitmap bitmap = BitmapFactory.decodeByteArray(specie.getImage(), 0, specie.getImage().length);
                holder.specieImage.setImageBitmap(bitmap);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            return convertView;
        }
    }

    class ViewHolder {


        ImageView specieImage;
        TextView tittle;
        TextView subtittle;


    }


    /**********************************************************************************************/

    public class DownloadImageSpecie extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                getImageSpecie();

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (specieAdapter.getCount() > 0) {
                gridView.setAdapter(specieAdapter);
            }

            progressDialog.cancel();
        }
    }


} // FIN DE LA CLASE SPECIE FRAGMENT
