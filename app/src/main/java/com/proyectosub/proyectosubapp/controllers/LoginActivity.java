package com.proyectosub.proyectosubapp.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.proyectosub.proyectosubapp.App;
import com.proyectosub.proyectosubapp.MainActivity;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.SharedPrefUtils;
import com.proyectosub.proyectosubapp.models.Success;
import com.proyectosub.proyectosubapp.models.UserMobile;
import com.proyectosub.proyectosubapp.restApi.RestApi;
import com.proyectosub.proyectosubapp.restApi.RestApiAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btnRegister;
    private Button btnLogin;
    private Button btnGuest;
    private EditText etUsername;
    private EditText etPassword;
    private String username;
    private String password;
    private RestApi restApi;

    private static final String KEY_EMPTY = "";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";

    private Button.OnClickListener clickRegister = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(i);
            finish();

        }
    };

    private Button.OnClickListener clickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            username = etUsername.getText().toString().toLowerCase().trim();
            password = etPassword.getText().toString().trim();
            if (validateInputs()) {
                login();
            }
        }
    };

    private Button.OnClickListener clickGuest = new View.OnClickListener() {
        @Override
        public void onClick(View view) {


            // configura menu

            SharedPrefUtils.put(getApplicationContext(), "username", "null");
            SharedPrefUtils.put(getApplicationContext(), "password", "null");

            SharedPrefUtils.put(getApplicationContext(), "id", "null");
            SharedPrefUtils.put(getApplicationContext(), "name", "null");
            SharedPrefUtils.put(getApplicationContext(), "surname", "null");
            SharedPrefUtils.put(getApplicationContext(), "email", "null");
            SharedPrefUtils.put(getApplicationContext(), "point", "null");
            SharedPrefUtils.put(getApplicationContext(), "connection", "3");


            Toast toast = Toast.makeText(getApplicationContext(), "no muestra perfil", Toast.LENGTH_SHORT);
            toast.show();


            String[] partesToken = {"null", "null"};

            SharedPrefUtils.put(getApplicationContext(), "token", partesToken[0]);
            SharedPrefUtils.put(getApplicationContext(), "token2", partesToken[1]);
            //

            //App.getInstance().setGoodConecction(true);

            /*Intent itemintent = new Intent(LoginActivity.this, MainActivity.class);
            LoginActivity.this.startActivity(itemintent);
            finish();*/
            loadDashboard();

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("ENTRE","ENTRENETERASDASD");

        setContentView(R.layout.activity_login);

        init();

        btnRegister.setOnClickListener(clickRegister);
        btnLogin.setOnClickListener(clickLogin);
        btnGuest.setOnClickListener(clickGuest);

    }

    private void init() {
        btnRegister = findViewById(R.id.btnLoginRegister);
        btnLogin = findViewById(R.id.btnLogin);
        etUsername = findViewById(R.id.etLoginUsername);
        etPassword = findViewById(R.id.etLoginPassword);
        btnGuest = findViewById(R.id.login_guest);
        restApi = new RestApiAdapter().connectionEnable();


//        Log.d("HAy datos guardados:", SharedPrefUtils.get(getApplicationContext(), "username") );


    }

    private boolean validateInputs() {
        if (KEY_EMPTY.equals(username)) {
            etUsername.setError(getResources().getString(R.string.username_empty));
            etUsername.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(password)) {
            etPassword.setError(getResources().getString(R.string.password_empty));
            etPassword.requestFocus();
            return false;
        }
        return true;
    }


    private void login() {
        Call<Success> call = restApi.login(username, password);
        call.enqueue(new Callback<Success>() {
            @Override
            public void onResponse(Call<Success> call, Response<Success> response) {
                Log.d("onResponse", String.valueOf(response.code()));
                if (response.code() == 200) {
                    SharedPrefUtils.put(getApplicationContext(), "username", username);
                    SharedPrefUtils.put(getApplicationContext(), "password", password);

                    Toast toast = Toast.makeText(getApplicationContext(), "Te logueaste :D", Toast.LENGTH_SHORT);
                    toast.show();

                    Success success = response.body();
                    Log.d("TOKEN2", success.success.token);
                    String c = success.success.token;
                    Log.d("string a cortar", c);
                    String[] partesToken = c.split("\\.");

                    SharedPrefUtils.put(getApplicationContext(), "token", partesToken[0]);
                    SharedPrefUtils.put(getApplicationContext(), "token2", partesToken[1]);
                    Log.d("primer parte: ", "todo ok no murio");
                    //Success.Token token = success.Token.token;

                    traerDatos();


                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Usuario y contraseña incorrecta", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }

            @Override
            public void onFailure(Call<Success> call, Throwable t) {
                Log.d("onFailure", t.toString());
                Toast toast = Toast.makeText(getApplicationContext(), "Error del sistema", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }


    public void traerDatos() {

        String user = SharedPrefUtils.get(getApplicationContext(), "username");
        Log.d("USERNAME:", user);


        Call<UserMobile> call = restApi.dataUser(user);
        call.enqueue(new Callback<UserMobile>() {
            @Override

            public void onResponse(Call<UserMobile> call, Response<UserMobile> response) {


                UserMobile d = response.body();

                SharedPrefUtils.put(getApplicationContext(), "id", d.data.id);
                SharedPrefUtils.put(getApplicationContext(), "name", d.data.name);
                SharedPrefUtils.put(getApplicationContext(), "surname", d.data.surname);
                SharedPrefUtils.put(getApplicationContext(), "email", d.data.email);


                SharedPrefUtils.put(getApplicationContext(), "point", String.valueOf(d.data.point));
                SharedPrefUtils.put(getApplicationContext(), "connection", "3");

                loadDashboard();
            }

            @Override
            public void onFailure(Call<UserMobile> call, Throwable t) {
                Log.d("onFailure", t.toString());
                Toast toast = Toast.makeText(getApplicationContext(), "error al traer info", Toast.LENGTH_SHORT);
                toast.show();
            }
        });


    } // fin del traer datos


    private void loadDashboard() {
        App.getInstance().setGoodConecction(true);
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();


    }

    /******************************************************************************/

    /**public class AsyncLogin extends AsyncTask<Void, Void, Void> {
     ProgressDialog pDialog;

     @Override protected Void doInBackground(Void... voids) {
     login();

     return null;
     }

     @Override protected void onPreExecute() {
     pDialog = new ProgressDialog(LoginActivity.this);
     pDialog.setMessage(getString(R.string.Logging));
     pDialog.setIndeterminate(false);
     pDialog.setCancelable(false);
     pDialog.show();
     }

     @Override protected void onPostExecute(Void aVoid) {
     pDialog.cancel();
     }

     @Override protected void onCancelled() {
     cancel(true);
     Toast toast = Toast.makeText(getApplicationContext(), "Usuario y contraseña incorrecta", Toast.LENGTH_SHORT);
     toast.show();
     }
     }*/
}

