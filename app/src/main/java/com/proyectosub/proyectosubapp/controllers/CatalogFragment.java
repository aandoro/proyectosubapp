package com.proyectosub.proyectosubapp.controllers;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.proyectosub.proyectosubapp.Adapters.RecycleViewGroup;
import com.proyectosub.proyectosubapp.DAO.GroupDAO;
import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.Utils.AppDatabase;
import com.proyectosub.proyectosubapp.Utils.Connection;
import com.proyectosub.proyectosubapp.models.Group;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CatalogFragment extends Fragment {

    private View catalogView;
    private List<Group> listGroup;
    private DownloadGroups downloadGroups;
    private OkHttpClient client;
    private RecyclerView recyclerView;
    private RecycleViewGroup adapter;
    private GroupDAO groupDAO;
    private AppDatabase database;
    private TextView groupsInv;

    public static CatalogFragment newInstance() {
        return new CatalogFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        catalogView = inflater.inflate(R.layout.catalog_fragment, container, false);

        init();
        downloadGroups.execute();

        return catalogView;
    }

    @SuppressLint("WrongConstant")
    private void init() {
        client = new OkHttpClient();
        this.listGroup = new ArrayList<>();
        this.downloadGroups = new DownloadGroups();
        recyclerView = catalogView.findViewById(R.id.group_list);
        database = Room.databaseBuilder(catalogView.getContext(), AppDatabase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();
        groupDAO = database.getGroupDAO();
        groupsInv = (TextView)catalogView.findViewById(R.id.titleGroups);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(catalogView.getContext());
        recyclerView.setLayoutManager(manager);
    }

    private void getGroups(String url) throws IOException, JSONException {

        Request request = new Request.Builder()
                .url(url + "groups")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
        }
        String body = response.body().string();
        JSONObject jsonObject = new JSONObject(body);
        JSONArray jsonArray = jsonObject.getJSONArray("data");

        for (int i = 0; i < jsonArray.length(); i++) {
            Group group = new Group();
            group.setId(jsonArray.getJSONObject(i).getInt("id"));
            group.setName(jsonArray.getJSONObject(i).getString("name"));
            group.setDescription(jsonArray.getJSONObject(i).getString("description"));

            try {
                groupDAO.insert(group);
            } catch (Exception e) {
                Log.d("CatalogFragment", e.toString());
            }

            listGroup.add(group);


        }
    }

    /**********************************************************************************************/

    public class DownloadGroups extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                getGroups(Connection.URL_WEB_SERVICES);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                listGroup = groupDAO.getGroups();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.loading));
            progressDialog.setMessage(getString(R.string.wait_please));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //adapter = new RecycleViewGroup(getActivity(), catalogView.getContext(), listGroup);
            groupsInv.setText(R.string.group_invertebrates);
            adapter = new RecycleViewGroup(getActivity(), catalogView.getContext(), listGroup);
            recyclerView.setAdapter(adapter);
            Log.d("DL", listGroup.toString());
            progressDialog.cancel();
        }
    }

}
