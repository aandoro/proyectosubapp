package com.proyectosub.proyectosubapp.controllers;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.models.Specie;


public class DetailsSpecieFragment extends Fragment {


    private View infoSpecie;
    private Specie specie;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        infoSpecie = inflater.inflate(R.layout.fragment_details_specie, container, false);

        init();
        traerDatos();

        return infoSpecie;


    }

    private void init() {

    }


    private void traerDatos() {
        TextView nameSpecie = (TextView) infoSpecie.findViewById(R.id.nameSpecie);
        TextView nameScientificSpecie = (TextView) infoSpecie.findViewById(R.id.nameScientificSpecie);
        TextView descriptionSpecie = (TextView) infoSpecie.findViewById(R.id.descriptionSpecie);
        TextView dietSpecie = (TextView) infoSpecie.findViewById(R.id.dietSpecie);
        TextView habitatSpecie = (TextView) infoSpecie.findViewById(R.id.habitatSpecie);
        TextView reproductionSpecie = (TextView) infoSpecie.findViewById(R.id.reproductionSpecie);
        ImageView imageSpecie = (ImageView) infoSpecie.findViewById(R.id.imageSpe);


        nameSpecie.setText(specie.getName());

        if (specie.getScientificName().isEmpty()) {

            nameScientificSpecie.append("Sin nombre.");
        } else {

            nameScientificSpecie.append(specie.getScientificName());
        }

        descriptionSpecie.append(specie.getDescription());
        dietSpecie.append(specie.getDiet());
        habitatSpecie.append(specie.getHabitat());
        reproductionSpecie.append(specie.getReproduction());
        Bitmap bitmap = BitmapFactory.decodeByteArray(specie.getImage(), 0, specie.getImage().length);
        imageSpecie.setImageBitmap(bitmap);


    }


    public Specie getSpecie() {
        return specie;
    }

    public void setSpecie(Specie specie) {
        this.specie = specie;
    }


}
