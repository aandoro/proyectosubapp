package com.proyectosub.proyectosubapp.models;

public class Games {

    private Challenge challenge;
    private Trivia trivia;
    private String label;

    public Games() {
    }

    public Challenge getChallenge() {
        return challenge;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }

    public Trivia getTrivia() {
        return trivia;
    }

    public void setTrivia(Trivia trivia) {
        this.trivia = trivia;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Games{" +
                "challenge=" + challenge +
                ", trivia=" + trivia +
                ", label='" + label + '\'' +
                '}';
    }
}
