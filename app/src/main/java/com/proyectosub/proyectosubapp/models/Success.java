package com.proyectosub.proyectosubapp.models;

import com.google.gson.annotations.SerializedName;

public class Success {

    @SerializedName("success")
    public Token success;

    public Success(Token success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "Success{" +
                "success=" + success +
                '}';
    }

    public class Token {
        @SerializedName("token")
        public String token;

        public Token(String token) {
            this.token = token;
        }

        @Override
        public String toString() {
            return "Token{" +
                    "token='" + token + '\'' +
                    '}';
        }
    }
}
