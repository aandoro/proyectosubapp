package com.proyectosub.proyectosubapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "levels",indices = {@Index(value = {"number"},
        unique = true)})
public class Level {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    @ColumnInfo(name = "number")
    private int number;
    private  int scoreRequired;

    @SerializedName("data")
    @Ignore
    public Data[] data;

    public Level() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getScoreRequired() {
        return scoreRequired;
    }

    public void setScoreRequired(int scoreRequired) {
        this.scoreRequired = scoreRequired;
    }

    @Override
    public String toString() {
        return "Level{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", number=" + number + '\'' +
                ", score Required=" + scoreRequired +
                '}';
    }


    public class Data {

        @SerializedName("id")
        public int id;

        @SerializedName("name")
        public String name;

        @SerializedName("number")
        public int number;

        @SerializedName("scoreRequired")
        public int scoreRequired;



        public Data(int id, String name, int number, int scoreRequired) {

            this.id = id;
            this.name = name;
            this.number = number;
            this.scoreRequired = scoreRequired;

        }

        @Override
        public String toString() {
            return "Data{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", number='" + number + '\'' +
                    ", scoreRequired=" + scoreRequired +
                    '}';
        }
    }




}
