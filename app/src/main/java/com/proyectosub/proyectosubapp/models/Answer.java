package com.proyectosub.proyectosubapp.models;

import java.io.Serializable;
import java.util.Date;

public class Answer implements Serializable {
    private int id;
    private float longitud;
    private float latitud;
    private Date init_challenge;
    private Date end_challenge;
    private String imageUri;
    private boolean sent;
    private Challenge challenge;



    public Answer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public Date getInit_challenge() {
        return init_challenge;
    }

    public void setInit_challenge(Date init_challenge) {
        this.init_challenge = init_challenge;
    }

    public Date getEnd_challenge() {
        return end_challenge;
    }

    public void setEnd_challenge(Date end_challenge) {
        this.end_challenge = end_challenge;
    }

    public Challenge getChallenge() {
        return challenge;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id=" + id +
                ", longitud=" + longitud +
                ", latitud=" + latitud +
                ", init_challenge=" + init_challenge +
                ", end_challenge=" + end_challenge +
                ", imageUri='" + imageUri + '\'' +
                ", sent=" + sent +
                ", challenge=" + challenge +
                '}';
    }
}
