package com.proyectosub.proyectosubapp.models;

import com.google.gson.annotations.SerializedName;

public class UserMobile {


    private int id;
    private String name;
    private String surname;
    private String username;
    private int point;
    private String email;
    private String password;


    @SerializedName("data")
    public Data data;



    public UserMobile() { }


    public UserMobile(Data data) {
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }




    @Override
    public String toString() {
        return "UserMobile{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", point=" + point +
                '}';
    }



    public class Data {

        @SerializedName("id")
        public String id;

        @SerializedName("name")
        public String name;

        @SerializedName("surname")
        public String surname;

        @SerializedName("username")
        public String username;

        @SerializedName("email")
        public String email;

        @SerializedName("point")
        public int point;


        public Data(String id, String name, String surname,String username, String email, int point) {

            this.id = id;
            this.name = name;
            this.surname = surname;
            this.username = username;
            this.email = email;
            this.point = point;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", username='" + username + '\'' +
                    ", email='" + email + '\'' +
                    ", point=" + point +
                    '}';
        }
    }




} // fin clase userMobile
