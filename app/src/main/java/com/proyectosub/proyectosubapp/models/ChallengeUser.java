package com.proyectosub.proyectosubapp.models;

import java.util.Objects;

public class ChallengeUser {

    private String init_challenge;
    private String finish_challenge;
    private String challenge_id;
    private String user_id;

    public ChallengeUser() {
    }

    public String getInit_challenge() {
        return init_challenge;
    }

    public void setInit_challenge(String init_challenge) {
        this.init_challenge = init_challenge;
    }

    public String getFinish_challenge() {
        return finish_challenge;
    }

    public void setFinish_challenge(String finish_challenge) {
        this.finish_challenge = finish_challenge;
    }

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChallengeUser that = (ChallengeUser) o;
        return challenge_id == that.challenge_id &&
                user_id == that.user_id &&
                Objects.equals(init_challenge, that.init_challenge) &&
                Objects.equals(finish_challenge, that.finish_challenge);
    }

    @Override
    public int hashCode() {
        return Objects.hash(init_challenge, finish_challenge, challenge_id, user_id);
    }
}
