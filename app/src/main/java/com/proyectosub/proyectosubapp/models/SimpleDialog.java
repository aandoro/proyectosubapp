package com.proyectosub.proyectosubapp.models;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.proyectosub.proyectosubapp.R;
import com.proyectosub.proyectosubapp.controllers.ChallengeFragment;
import com.proyectosub.proyectosubapp.controllers.TriviaFragment;

public class SimpleDialog extends DialogFragment {


    private String tituloDelJuego;
    private String descripcionDelJuego;
    private int idGame, picCount;
    private String labelGame;

    public SimpleDialog() {
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createSimpleDialog();
    }

    /**
     * Crea un diálogo de alerta sencillo
     *
     * @return Nuevo diálogo
     */
    public AlertDialog createSimpleDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(tituloDelJuego)
                .setMessage(descripcionDelJuego)
                .setPositiveButton(R.string.dialog_accept,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("LABELGAME", labelGame);
                                if (labelGame.equals("Trivia")) {
                                    abrirTrivia();
                                } else {
                                    openChallenge();
                                }
                            }
                        })
                .setNegativeButton(R.string.dialog_cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dismiss(); //descarta el fragmento


                            }
                        });

        return builder.create();
    }

    public void abrirTrivia() {
        Fragment fragment = new TriviaFragment();
        ((TriviaFragment) fragment).setTriviaId(this.idGame);
        replaceFragment(fragment);

    }

    public void openChallenge() {
        Fragment fragment = new ChallengeFragment();
        ((ChallengeFragment) fragment).setChallengeId(this.idGame);
        ((ChallengeFragment) fragment).setPicCountChallenge(this.picCount);
        replaceFragment(fragment);
    }


    private void replaceFragment(Fragment fragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);

        // Commit a la transacción
        transaction.commit();
    }

    public int getIdGame() {
        return idGame;
    }

    public void setIdGame(int idGame) {
        this.idGame = idGame;
    }

    public String getLabelGame() {
        return labelGame;
    }

    public void setLabelGame(String label) {
        labelGame = label;
    }

    public void setTituloDelJuego(String tituloDelJuego) {
        this.tituloDelJuego = tituloDelJuego;
    }

    public void setDescripcionDelJuego(String descripcionDelJuego) {
        this.descripcionDelJuego = descripcionDelJuego;
    }

    public int getPicCount() {
        return picCount;
    }

    public void setPicCount(int picCount) {
        this.picCount = picCount;
    }

    public String getDescripcionDelJuego() {
        return descripcionDelJuego;
    }

    public String getTituloDelJuego() {

        return tituloDelJuego;
    }

    @Override
    public String toString() {
        return "SimpleDialog{" +
                "tituloDelJuego='" + tituloDelJuego + '\'' +
                ", descripcionDelJuego='" + descripcionDelJuego + '\'' +
                ", idGame=" + idGame +
                ", labelGame='" + labelGame + '\'' +
                '}';
    }
}