package com.proyectosub.proyectosubapp.models;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "options", foreignKeys = @ForeignKey(entity = Trivia.class,
        parentColumns = "id",
        childColumns = "triviaId",
        onDelete = CASCADE))
public class Option {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String option;
    private int triviaId;

    public Option() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public int getTriviaId() {
        return triviaId;
    }

    public void setTriviaId(int triviaId) {
        this.triviaId = triviaId;
    }

    @Override
    public String toString() {
        return "Option{" +
                "id=" + id +
                ", option='" + option + '\'' +
                ", trivia=" + triviaId +
                '}';
    }
}
