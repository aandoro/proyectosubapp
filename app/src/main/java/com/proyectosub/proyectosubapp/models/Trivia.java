package com.proyectosub.proyectosubapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "trivias", indices = {@Index(value = {"name"},
        unique = true)}, foreignKeys = @ForeignKey(entity = Level.class,
        parentColumns = "id",
        childColumns = "levelId",
        onDelete = CASCADE))
public class Trivia {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "name")
    private String name;
    private String description;
    private String question;
    private String optCorrect;
    private int point;
    private int levelId;
    @Ignore
    private Category category;

    @SerializedName("data")
    @Ignore
    public Data[] data;

    public Trivia() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptCorrect() {
        return optCorrect;
    }

    public void setOptCorrect(String optCorrect) {
        this.optCorrect = optCorrect;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int level) {
        this.levelId = level;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Trivia{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", question='" + question + '\'' +
                ", optCorrect='" + optCorrect + '\'' +
                ", point=" + point +
                ", level=" + levelId +
                ", category=" + category +
                '}';
    }



    public class Data {

        @SerializedName("id")
        public int id;

        @SerializedName("name")
        public String name;

        @SerializedName("description")
        public String description;

        @SerializedName("question")
        public String question;

        @SerializedName("optCorrect")
        public String optCorrect;

        @SerializedName("point")
        public int point;

        @SerializedName("level_id")
        public int level_id;

        @SerializedName("category_id")
        public int category_id;


        public Data(int id, String name, String description, String question,String optCorrect,  int point, int level, int category) {

            this.id = id;
            this.name = name;
            this.description = description;
            this.question = question;
            this.optCorrect = optCorrect;
            this.point = point;
            this.level_id = level;
            this.category_id = category;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", description='" + description + '\'' +
                    ", question='" + question + '\'' +
                    ", optCorrect='" + optCorrect + '\'' +
                    ", point=" + point +
                    ", level_id=" + level_id +
                    ", category_id=" + category_id +
                    '}';
        }
    }







}
