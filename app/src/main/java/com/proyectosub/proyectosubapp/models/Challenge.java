package com.proyectosub.proyectosubapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "challenges", indices = {@Index(value = {"name"},
        unique = true)}, foreignKeys = @ForeignKey(entity = Level.class,
        parentColumns = "id",
        childColumns = "levelId",
        onDelete = CASCADE))
public class Challenge {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "name")
    private String name;
    private String description;
    private int point;
    private int picCount;
    private int levelId;
    @Ignore
    private Category category;

    @SerializedName("data")
    @Ignore
    public Data[] data;

    public Challenge() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getPicCount() {
        return picCount;
    }

    public void setPicCount(int picCount) {
        this.picCount = picCount;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Challenge{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", point=" + point +
                ", picCount=" + picCount +
                ", level=" + levelId +
                ", category=" + category +
                '}';
    }


    public class Data {

        @SerializedName("id")
        public int id;

        @SerializedName("name")
        public String name;

        @SerializedName("description")
        public String description;

        @SerializedName("point")
        public int point;

        @SerializedName("picCount")
        public int picCount;

        @SerializedName("level")
        public int level;

        @SerializedName("category")
        public int category;


        public Data(int id, String name, String description, int point, int picCount, int level, int category) {

            this.id = id;
            this.name = name;
            this.description = description;
            this.point = point;
            this.picCount = picCount;
            this.level = level;
            this.category = category;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", description='" + description + '\'' +
                    ", point=" + point +
                    ", picCount=" + picCount +
                    ", level=" + level +
                    ", category=" + category +
                    '}';
        }
    }


}
