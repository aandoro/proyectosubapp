package com.proyectosub.proyectosubapp.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "locations")
public class MyLocation {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private double latitud;
    private double longitud;
    private String nameFile;

    public MyLocation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    @Override
    public String toString() {
        return "MyLocation{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                ", nameFile=" + nameFile +
                '}';
    }
}
